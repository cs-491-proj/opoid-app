import { initializeApp } from 'firebase/app'
import {
    getFirestore, //tpye of database
    collection, //same as a column for mysql 
    onSnapshot, //same as getDocs but it does it without the need to reload page 
    getDocs,//read documents in collections
    query, //get certain sections in database 
    where, //an event to find things in getDocs 
    serverTimestamp, //gets the time when user does stuff
    addDoc, // add documents to collections with auto ids 
    setDoc, // for custom ids 
    doc, //for custom ids 
} from 'firebase/firestore'

import {
    getAuth, //login/logout in authentication 
    signInWithPopup, //the pop up will appear
    GoogleAuthProvider, // sign up with google 
    signOut, //loging out users 
    onAuthStateChanged, //checks when the user logs in and out 
} from 'firebase/auth'

import { 
    getAnalytics , 
    logEvent //events logged to firebase analytics 
} from "firebase/analytics";

//Real 491 project 
const firebaseConfig = {
    apiKey: "AIzaSyBuOmHXmSI6k2uERnSDPeMcz19piu4XvXs",
    authDomain: "opioid-f0673.firebaseapp.com",
    projectId: "opioid-f0673",
    storageBucket: "opioid-f0673.appspot.com",
    messagingSenderId: "42171715859",
    appId: "1:42171715859:web:1e1c2749d0175365de4ae6",
    measurementId: "G-S17FZRWB3Y" // For Firebase JS SDK v7.20.0 and later, measurementId is optional
};

// init firebase
const app = initializeApp(firebaseConfig)

// computer's current time  
const today = new Date()
var h = today.getHours()

// init services
const db = getFirestore(app)
const auth = getAuth(app) // authentication
const provider = new GoogleAuthProvider(app)
const analytics = getAnalytics(app)

// collection ref
const colRef = collection(db, 'signup-users')

//real time data collection,post automatically to firestore database 
onSnapshot(colRef, (snapshot) => {
    let userdata = []
    snapshot.docs.forEach(doc => {
        userdata.push({...doc.data(), id: doc.id })
    })
    //console.log(userdata)
})



// Version 4: signup new users, logged to firebase analytics as sign_up 
const addSignupForm = document.querySelector('.add')
    addSignupForm.addEventListener('submit', (e) => {
    e.preventDefault()

    signInWithPopup(auth, provider)//Redirect does not work nothing saves to db 

    .then((result) => {
      const user = result.user
      //console.log(user)

      var getGender = addSignupForm.gender.value 

      if (getGender == "Other"){ //When user picks other as a gender 
       var otherGender = addSignupForm.other.value //holds the real gender of user 
        getGender = otherGender 
        //console.log("GENDER:",getGender) //error check
      }
      else{
        //console.log("GENDER NOT OTHER")//error check
      }
      
      const docData= {
        phone: addSignupForm.phone.value,
        age: addSignupForm.age.value,
        annual_income: addSignupForm.annual_income.value,
        comorbid_conditions: addSignupForm.comorbid_conditions.value,
        current_employment_status: addSignupForm.current_employment_status.value,
        current_health_insurance_status: addSignupForm.current_health_insurance_status.value,
        current_marital_status: addSignupForm.current_marital_status.value,
        education: addSignupForm.education.value,
        ethnicity: addSignupForm.ethnicity.value,
        gender: getGender,
        general_self_rated_health: addSignupForm.general_self_rated_health.value,
        health_literacy: addSignupForm.health_literacy.value,
        height: addSignupForm.height.value,
        weight: addSignupForm.weight.value,
       
        social_isolaton_1: addSignupForm.social_isolaton_1.value,
        social_isolaton_2: addSignupForm.social_isolaton_2.value,
        social_isolaton_3: addSignupForm.social_isolaton_3.value,
        social_isolaton_4: addSignupForm.social_isolaton_4.value,
        social_isolaton_5: addSignupForm.social_isolaton_5.value,
        social_isolaton_6: addSignupForm.social_isolaton_6.value,
        social_isolaton_7: addSignupForm.social_isolaton_7.value,
        social_isolaton_8: addSignupForm.social_isolaton_8.value,
        social_isolaton_9: addSignupForm.social_isolaton_9.value,
        social_isolaton_10: addSignupForm.social_isolaton_10.value,
        social_isolaton_11: addSignupForm.social_isolaton_11.value,
        social_isolaton_12: addSignupForm.social_isolaton_12.value,
        social_isolaton_13: addSignupForm.social_isolaton_13.value,
        social_isolaton_14: addSignupForm.social_isolaton_14.value,
        social_isolaton_15: addSignupForm.social_isolaton_15.value,
        social_isolaton_16: addSignupForm.social_isolaton_16.value,
        social_isolaton_17: addSignupForm.social_isolaton_17.value,
        social_isolaton_18: addSignupForm.social_isolaton_18.value,
        social_isolaton_19: addSignupForm.social_isolaton_19.value,
        social_isolaton_20: addSignupForm.social_isolaton_20.value,

        risk_for_opioid_misuse_1: addSignupForm.risk_for_opioid_misuse_1.value,
        risk_for_opioid_misuse_2: addSignupForm.risk_for_opioid_misuse_2.value,
        risk_for_opioid_misuse_3: addSignupForm.risk_for_opioid_misuse_3.value,
        risk_for_opioid_misuse_4: addSignupForm.risk_for_opioid_misuse_4.value,
        risk_for_opioid_misuse_5: addSignupForm.risk_for_opioid_misuse_5.value,
        risk_for_opioid_misuse_6: addSignupForm.risk_for_opioid_misuse_6.value,
        risk_for_opioid_misuse_7: addSignupForm.risk_for_opioid_misuse_7.value,
        risk_for_opioid_misuse_8: addSignupForm.risk_for_opioid_misuse_8.value,
        risk_for_opioid_misuse_9: addSignupForm.risk_for_opioid_misuse_9.value,
        risk_for_opioid_misuse_10: addSignupForm.risk_for_opioid_misuse_10.value,
        risk_for_opioid_misuse_11: addSignupForm.risk_for_opioid_misuse_11.value,
        risk_for_opioid_misuse_12: addSignupForm.risk_for_opioid_misuse_12.value,
        risk_for_opioid_misuse_13: addSignupForm.risk_for_opioid_misuse_13.value,

        createdAt: serverTimestamp(),
        name: user.displayName,
    }
      var email = user.email
      var emailType = provider.providerId //for logEvent

      //Check if user already have ancount ? 
      const q = query(collection(db,'signup-users'), where("createdAt","!=",""))
      getDocs(q).then((docSnap) => {
        let oldUserEmail = [];
        docSnap.forEach((doc) => {
          oldUserEmail.push(doc.id)
        })
        //console.log(oldUserEmail)
        var sameCount = 0
        for (let i = 0; i < oldUserEmail.length; i++) {
          if (email == oldUserEmail[i]) { //found one old email thats the same as new email 
            sameCount = 1  
          } 
        }
        if (sameCount != 1){
          //console.log('SNED: '+email)
          logEvent(analytics, 'sign_up', {
            method: emailType
          })
          setDoc(doc(db,'signup-users/',email),docData) //makes custom ids as emails 
        }else{ 
          //console.log('NOT SEND: '+email)
          alert("You already have an account")
        }
      })
 
      //var phoneNum = addSignupForm.phone.value
      //setDoc(doc(db,'signup-users/',phoneNum),docData) //makes custom ids as phone numbers 
      addSignupForm.reset()//clears the page 
    })
})


//Version 3: add Morning form question, now with analytics
const unsubAuthMorning = onAuthStateChanged(auth, (user) => {
    if (user != null) {
        const morningForm = document.querySelector('.morningAdd')
        morningForm.addEventListener('submit', (e) => {
            e.preventDefault()

            var timeStamp = String(new Date()) //date off user computer 
            var email = user.email
            
             logEvent(analytics, 'morning_submits', {  
              userEmail: email
            })

            const docData = {
                createdAt: serverTimestamp(),
                quality_sleep: morningForm.sleep.value,
            }
            setDoc(doc(db, 'signup-users/', email, '/morning_questions/', timeStamp), docData) //doc(db,'collection/',id,'/subcollection',subid)

            morningForm.reset() //clears the page 

            setTimeout(function() {
                    signOut(auth)
                    window.location = 'index.html'
                }, 5000) // 5 sec delay
        })

    } else {
        //console.log('user status changed: ', user)//tells when the user logs in and out 
        signOut(auth)
            //window.location = 'index.html' //goes to home page 
    }
})


// Version1: logging out 
const logoutButton = document.querySelector('.logout')

if (logoutButton) {
    logoutButton.addEventListener('click', () => {
        signOut(auth)
            .then(() => {
                // console.log('User signed out')//old log check
                window.location = 'index.html' //goes to home page
            })
            .catch((err) => {
                //console.log(err.message)
            })
    })
}

const registerFunction = document.querySelector('.register')
registerFunction.addEventListener('click', (e) => {
    e.preventDefault()
    console.log("user pressed register")
    window.location = "signup.html"
})


//Version 4: user logs into their account, logged to firebase analytics as login 
const loginFuntion = document.querySelector('.login')
    loginFuntion.addEventListener('click', (e) => {
    e.preventDefault()
    
    signInWithPopup(auth, provider)//Redirect does not work nothing saves to db 

    .then((result) => {
      const user = result.user
      //console.log(user)

      var emailType = provider.providerId //for logEvent
      var email = user.email 
      logEvent(analytics, 'login', {
        method: emailType
      })

      //Check if user already have ancount ? 
      const q = query(collection(db,'signup-users'), where("createdAt","!=",""))
      getDocs(q).then((docSnap) => {
        let oldUserEmail = [];
        docSnap.forEach((doc) => {
          oldUserEmail.push(doc.id)
        })
        //console.log(oldUserEmail)
        var sameCount = 0
        for (let i = 0; i < oldUserEmail.length; i++) {
          if (email == oldUserEmail[i]) { //found one old email thats the same as new email 
            sameCount = 1  
          } 
        }
        if (sameCount == 1){
          //console.log('SNED: '+email)
            var hour = h //using the computer time 
      
            if (hour < 18){ //morning 12am to 6pm
            //if (hour = 17){
              window.location = 'morningQuestionForm.html'
            }  
            if(hour >= 18){//evening 6pm  
              window.location = 'eveningQuestionForm.html'
            }

        }else{
          //console.log('NOT SEND: '+email)
          alert("You don't have an account.")
          signOut(auth)
        }
      })
    
    })
})


//Version3: add Evening form question, now with analytics
const unsubAuthEvening = onAuthStateChanged(auth, (user) => {
    if (user != null) {
        const eveningForm = document.querySelector('.eveningAdd')
        eveningForm.addEventListener('submit', (e) => {
            e.preventDefault()

            var timeStamp = String(new Date()) //date off user computer 
            var email = user.email
            
            logEvent(analytics, 'evening_submits', {  
              userEmail: email
            })

            const docData = {
                alcohol_consumption: eveningForm.alcohol_consumption.value,
                mood: eveningForm.mood.value,

                exercise: eveningForm.exercise.value,
                arryExerciseYes1: [eveningForm.exercise_time1.value, eveningForm.exercise_description1.value, eveningForm.duration1.value],
                arryExerciseYes2: [eveningForm.exercise_time2.value, eveningForm.exercise_description2.value, eveningForm.duration2.value],
                arryExerciseYes3: [eveningForm.exercise_time3.value, eveningForm.exercise_description3.value, eveningForm.duration3.value],
                arryExerciseYes4: [eveningForm.exercise_time4.value, eveningForm.exercise_description4.value, eveningForm.duration4.value],

                medicine: eveningForm.medicine.value,
                arryMedicineYes1: [eveningForm.medicine_time1.value, eveningForm.medicine_description1.value, eveningForm.pain1.value, eveningForm.non_opioid_description1.value],
                arryMedicineYes2: [eveningForm.medicine_time2.value, eveningForm.medicine_description2.value, eveningForm.pain2.value, eveningForm.non_opioid_description2.value],
                arryMedicineYes3: [eveningForm.medicine_time3.value, eveningForm.medicine_description3.value, eveningForm.pain3.value, eveningForm.non_opioid_description3.value],
                arryMedicineYes4: [eveningForm.medicine_time4.value, eveningForm.medicine_description4.value, eveningForm.pain4.value, eveningForm.non_opioid_description4.value],
                arryMedicineYes5: [eveningForm.medicine_time5.value, eveningForm.medicine_description5.value, eveningForm.pain5.value, eveningForm.non_opioid_description5.value],
                createdAt: serverTimestamp(),
            }
            setDoc(doc(db, 'signup-users/', email, '/evening_questions/', timeStamp), docData) //doc(db,'collection/',id,'/subcollection',subid)

            eveningForm.reset() //clears the page 

            setTimeout(function() {
                signOut(auth)
                window.location = 'index.html'
            }, 5000)
        })

    } else {
        //console.log('user status changed: ', user)//tells when the user logs in and out 
        signOut(auth)
            //window.location = 'index.html' //goes to home page 
    }
})

